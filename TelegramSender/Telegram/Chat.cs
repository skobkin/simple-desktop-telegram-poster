﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace TelegramSender.Telegram
{
    [Serializable()]
    class Chat
    {
        private string chatTitle;

        private long chatId;

        public Chat(long chatId, string chatTitle)
        {
            this.chatId = chatId;
            this.chatTitle = chatTitle;
        }

        public string ChatTitle { get => chatTitle; }
        public long ChatId { get => chatId; }

        public override string ToString()
        {
            return this.ChatTitle + "(" + this.chatId + ")";
        }
    }
}
