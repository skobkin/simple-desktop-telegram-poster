﻿using System;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace TelegramSender.Telegram
{
    class MessageSender
    {
        private TelegramBotClient telegram;

        public MessageSender(string token)
        {
            this.telegram = new TelegramBotClient(token);
        }

        public async Task<bool> SendMessageAsync(long chatId, string text, ParseMode parseMode)
        {
            ChatId chat = new ChatId(chatId);

            try
            {
                await telegram.SendTextMessageAsync(chat, text, parseMode);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
