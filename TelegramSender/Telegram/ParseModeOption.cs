﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.Enums;

namespace TelegramSender.Telegram
{
    class ParseModeOption
    {
        private string name;

        private int code;

        public ParseModeOption(int code, string name)
        {
            this.code = code;
            this.name = name;
        }

        public ParseMode ToEnum()
        {
            switch (code)
            {
                case 1:
                    return ParseMode.Markdown;

                case 2:
                    return ParseMode.Html;
                
                default:
                    return ParseMode.Default;
            }
        }

        public override string ToString()
        {
            return name;
        }
    }
}
