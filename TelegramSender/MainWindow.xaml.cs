﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TelegramSender.Settings;
using TelegramSender.Telegram;

namespace TelegramSender
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MessageSender messageSender;

        public MainWindow()
        {
            InitializeComponent();
        }

        public void Settings_Updated(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            InitMessageSender();
            LoadSettings();

            ParseModeSelect.Items.Add(new ParseModeOption(0, "Plain text"));
            ParseModeSelect.Items.Add(new ParseModeOption(1, "Markdown"));
            ParseModeSelect.Items.Add(new ParseModeOption(2, "HTML"));
            ParseModeSelect.SelectedIndex = 1;
        }

        private void SendButton_Click(object sender, RoutedEventArgs e)
        {
            if (MessageText.Text == string.Empty)
            {
                MessageBox.Show("Message is empty.", "Sender error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }

            if (!InitMessageSender())
            {
                MessageBox.Show("Message sender cannot be initialized. Check API token in the settings.", "Sender error", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return;
            }

            Chat chat = (Chat)ChatsSelect.SelectedItem;

            if (chat == null)
            {
                MessageBox.Show("Chat is not selected", "Sender error", MessageBoxButton.OK, MessageBoxImage.Exclamation);

                return;
            }

            ParseModeOption parseMode = (ParseModeOption)ParseModeSelect.Items.GetItemAt(ParseModeSelect.SelectedIndex);

            messageSender.SendMessageAsync(chat.ChatId, MessageText.Text, parseMode.ToEnum());
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            SettingsWindow settings = new SettingsWindow();
            settings.Updated += Settings_Updated;
            settings.Show();
        }

        private bool InitMessageSender()
        {
            if (messageSender != null)
            {
                return true;
            }

            string token = Properties.Settings.Default.TelegramBotToken;

            if (token != null && token != "")
            {
                messageSender = new MessageSender(token);

                return true;
            }

            return false;
        }

        private void LoadSettings()
        {
            Chat[] chatItems = ChatsOptionsSerializer.LoadChats();

            if (chatItems.Length > 0)
            {
                ChatsSelect.Items.Clear();

                foreach (Chat item in chatItems)
                {
                    ChatsSelect.Items.Add(item);
                }

                // Choosing first chat in the list
                ChatsSelect.SelectedIndex = 0;
            }
        }
    }
}
