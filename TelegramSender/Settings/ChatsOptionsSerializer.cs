﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TelegramSender.Telegram;

namespace TelegramSender.Settings
{
    static class ChatsOptionsSerializer
    {
        public static Chat[] LoadChats()
        {
            string chatsJson = Properties.Settings.Default.TelegramChats;
            Chat[] chats = new Chat[] { };

            if (chatsJson != null && chatsJson != "")
            {
                try
                {
                    chats = JsonConvert.DeserializeObject<Chat[]>(chatsJson);
                }
                catch (Exception)
                {

                }
            }

            return chats;
        }

        public static void SaveChats(Chat[] chats)
        {
            string chatsJson;

            chatsJson = JsonConvert.SerializeObject(chats);

            Properties.Settings.Default.TelegramChats = chatsJson;
        }
    }
}
