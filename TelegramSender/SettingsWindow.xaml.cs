﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized    ;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TelegramSender.Settings;
using TelegramSender.Telegram;

namespace TelegramSender
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow()
        {
            InitializeComponent();
        }

        public event EventHandler Updated;

        private void Settings_Initialized(object sender, EventArgs e)
        {
            TokenText.Password = Properties.Settings.Default.TelegramBotToken;

            Chat[] chats = ChatsOptionsSerializer.LoadChats();

            foreach (Chat chat in chats)
            {
                ChatList.Items.Add(chat);
            }
        }

        private void SaveSettingsButton_Click(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.TelegramBotToken = this.TokenText.Password;

            Chat[] chatListItems = new Chat[ChatList.Items.Count];
            ChatList.Items.CopyTo(chatListItems, 0);
            ChatsOptionsSerializer.SaveChats(chatListItems);

            // Saving settings
            Properties.Settings.Default.Save();

            // Calling "settings updated" event
            Updated(this, EventArgs.Empty);
        }

        private void AddChatButton_Click(object sender, RoutedEventArgs e)
        {
            Chat chat = CreateChatItemFromStrings(ChatIdText.Text, ChatTitleText.Text);

            this.ChatList.Items.Add(chat);
        }

        private void RemoveChatButton_Click(object sender, RoutedEventArgs e)
        {
            if ((ChatList.Items.Count > 0) && (this.ChatList.SelectedIndex >= 0))
            {
                this.ChatList.Items.RemoveAt(this.ChatList.SelectedIndex);
            }
        }

        private Chat CreateChatItemFromStrings(string id, string title)
        {
            long chatId;

            try
            {
                chatId = System.Convert.ToInt64(id);
            }
            catch (Exception)
            {
                MessageBox.Show("Incorrect chat ID", "Chat settings", MessageBoxButton.OK, MessageBoxImage.Error);

                return null;
            }

            if (title.Length == 0)
            {
                MessageBox.Show("Incorrect chat title", "Chat settings", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return new Chat(chatId, title);
        }
    }
}
